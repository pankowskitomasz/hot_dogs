import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass']
})
export class SidebarComponent implements OnInit {
  showFilters:boolean = true;

  showHide(){
    this.showFilters = !this.showFilters;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
