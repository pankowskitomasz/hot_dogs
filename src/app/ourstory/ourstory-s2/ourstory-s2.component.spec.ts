import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OurstoryS2Component } from './ourstory-s2.component';

describe('OurstoryS2Component', () => {
  let component: OurstoryS2Component;
  let fixture: ComponentFixture<OurstoryS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OurstoryS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OurstoryS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
