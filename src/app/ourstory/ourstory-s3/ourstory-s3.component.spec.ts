import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OurstoryS3Component } from './ourstory-s3.component';

describe('OurstoryS3Component', () => {
  let component: OurstoryS3Component;
  let fixture: ComponentFixture<OurstoryS3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OurstoryS3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OurstoryS3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
