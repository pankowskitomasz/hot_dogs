import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OurstoryS1Component } from './ourstory-s1.component';

describe('OurstoryS1Component', () => {
  let component: OurstoryS1Component;
  let fixture: ComponentFixture<OurstoryS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OurstoryS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OurstoryS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
