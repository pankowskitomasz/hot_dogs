import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OurstoryS4Component } from './ourstory-s4.component';

describe('OurstoryS4Component', () => {
  let component: OurstoryS4Component;
  let fixture: ComponentFixture<OurstoryS4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OurstoryS4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OurstoryS4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
