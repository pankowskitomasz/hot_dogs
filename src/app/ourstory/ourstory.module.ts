import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OurstoryRoutingModule } from './ourstory-routing.module';
import { OurstoryComponent } from './ourstory/ourstory.component';
import { OurstoryS1Component } from './ourstory-s1/ourstory-s1.component';
import { OurstoryS2Component } from './ourstory-s2/ourstory-s2.component';
import { OurstoryS3Component } from './ourstory-s3/ourstory-s3.component';
import { OurstoryS4Component } from './ourstory-s4/ourstory-s4.component';


@NgModule({
  declarations: [
    OurstoryComponent,
    OurstoryS1Component,
    OurstoryS2Component,
    OurstoryS3Component,
    OurstoryS4Component
  ],
  imports: [
    CommonModule,
    OurstoryRoutingModule
  ]
})
export class OurstoryModule { }
